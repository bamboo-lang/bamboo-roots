#ifndef __bamboo_roots_utils_cpp__
#define __bamboo_roots_utils_cpp__

#include <string>
#include <iostream>

namespace Utils
{
	inline void error(std::string message)
	{
		std::cout << "\033[31mError: \033[0m" << message << std::endl;
		exit(1);
	}

	inline void warn(std::string message)
	{
		std::cout << "\033[34mwarning: \033[0m" << message << std::endl;
	}
}

#endif
