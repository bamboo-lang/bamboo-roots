#include "utils.hpp"
#include <list>

const std::string HELP =
	"\033[1musage\033[0m: roots action [file]\n\n\
\033[1mactions\033[0m:\n\
    add           add a package\n\
    remove        remove a package\n\
    list          list installed packages\n\
    freeze        display all installed packages and save them to roots-lock.txt\n\
    env           create a virtual env to isolate packages\n\
    help          display this message";


struct RootsConfig
{
    std::string projectName;
    std::list<std::string> projectAuthors;
    std::string version;
    std::list<std::string> packages;
};


inline void displayHelp()
{
	std::cout << HELP << std::endl;
    exit(0);
}

void addPackage(std::string package)
{ Utils::warn("todo: implement addPackage"); }

void removePackage(std::string package)
{ Utils::warn("todo: implement removePackage"); }

void listPackages()
{ Utils::warn("todo: implement listPackages"); }

void freezePackages()
{ Utils::warn("todo: implement freezePackages"); }

void makeVirtualEnv()
{ Utils::warn("todo: implement makeVirtualEnv"); }


int main(int argc, char** argv)
{
	// check to make sure an action is provided
	if (argc == 1)
		Utils::error("roots: no action provided. See `roots help`");

    RootsConfig conf;

	std::string action = argv[1];

    if (action == "add")
        addPackage(argv[2]);
    else if (action == "remove")
        removePackage(argv[2]);
    else if (action == "list")
        listPackages();
    else if (action == "freeze")
        freezePackages();
    else if (action == "env")
        makeVirtualEnv();
    else if (action == "help")
        displayHelp();
    else
        Utils::error("roots: invalid action `" + action + "`. See `roots help`");
}
